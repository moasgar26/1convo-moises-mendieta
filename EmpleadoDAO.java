/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.ArrayList;
import java.util.List;
import pojo.Empleado;

/**
 *
 * @author samsung
 */
public class EmpleadoDAO {

    public List<Empleado> Empleados = new ArrayList();

    public void AnhadirEmpleado(Empleado e) {
        int CantEmpleados = Empleados.size();
        String codigo = "00" + CantEmpleados + 1;
        if (e != null) {
            Empleados.add(e);
        } else {
            System.out.println("Ingrese empleado");
        }
    }

    public boolean EditarEmpleado(Empleado e) {
        boolean flagEditado = false;
        String codigo = e.getCodigo();
        Empleado emp = BuscarPorCodigo(codigo);
        if (emp != null) {
            int indice = Empleados.indexOf(emp);
            Empleados.set(indice, e);
            flagEditado = true;
        } else {
            System.out.println("Empleado No Modificado");
        }
        
        return flagEditado;
    }

    public Empleado BuscarPorCodigo(String codigo) {
        boolean FlagEncontrado = false;
        Empleado emp = null;
        for (Empleado e : Empleados) {
            if (e.getCodigo().equalsIgnoreCase(codigo)) {
                System.out.println("Empleado Encontrado");
                System.out.println("Empleado: " + e.getNombres() + " " + e.getApellidos());
                System.out.println("Cédula:" + e.getCedula());
                FlagEncontrado = true;
                emp = e;
            }
            if (FlagEncontrado) {
                break;
            }
        }
        if (!FlagEncontrado) {
            System.out.println("Empleado No encontrado");
        }
        return emp;
    }

    public List<Empleado> BuscarPorApellidos(String apellidos) {
        boolean FlagEncontrado = false;
        List<Empleado> emps = new ArrayList();
        for (Empleado e : Empleados) {
            if (e.getApellidos().toUpperCase().contains(apellidos.toUpperCase())) {
                FlagEncontrado = true;
                emps.add(e);
            }
        }
        if (!FlagEncontrado) {
            System.out.println("Sin resultados");
        }
        return emps;
    }

    public boolean EliminarEmpleado(String codigo) {
        boolean flagEliminado = false;
        Empleado e = BuscarPorCodigo(codigo);

        int index = Empleados.indexOf(e);
        if (index >= 0) {
            Empleados.remove(index);
            flagEliminado = true;
        } else {
            System.out.println("El empleado no fue encontrado");
        }

        return flagEliminado;
    }
}
