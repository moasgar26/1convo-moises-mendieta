/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import DAO.EmpleadoDAO;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import pojo.Empleado;

/**
 *
 * @author samsung
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        EmpleadoDAO DAO = new EmpleadoDAO();
        int opcMenu = 0;
        int opcSeguir = 1;
        do {
            while (opcMenu != 7) {
                System.out.println("Sistema de Empleados");
                System.out.println("Elija su opción");
                System.out.println("1. Añadir Empleado");
                System.out.println("2. Editar Empleado");
                System.out.println("3. Eliminar Empleado");
                System.out.println("4. Buscar Empleado por Codigo");
                System.out.println("5. Buscar Empleado por Apellidos");
                System.out.println("6. Ver colillas de Empleados");

                opcMenu = sc.nextInt();

                if (opcMenu < 1 || opcMenu > 6) {
                    System.out.println("Digite una opción correcta");
                }
            }

            switch (opcMenu) {
                case 1: {
                    System.out.println("Nombres:");
                    String Nombre = sc.nextLine();
                    System.out.println("Apellidos: ");
                    String Apellidos = sc.nextLine();
                    System.out.println("Cédula:");
                    String cedula = sc.nextLine();
                    String fecha_Ingreso = sc.nextLine();
                    float Salario = sc.nextFloat();

                    Empleado e = new Empleado("", cedula, Nombre, Apellidos, fecha_Ingreso, Salario);
                    DAO.AnhadirEmpleado(e);
                    break;
                }

                case 2: {
                    System.out.println("Digite el codigo del empleado");
                    String codigo = sc.nextLine();
                    Empleado e = DAO.BuscarPorCodigo(codigo);

                    if (e != null) {
                        System.out.println("Nombres:");
                        String Nombre = sc.nextLine();
                        System.out.println("Apellidos: ");
                        String Apellidos = sc.nextLine();
                        System.out.println("Cédula:");
                        String cedula = sc.nextLine();
                        String fecha_Ingreso = sc.nextLine();
                        float Salario = sc.nextFloat();

                        Empleado emp = new Empleado(codigo, cedula, Nombre, Apellidos, fecha_Ingreso, Salario);
                        DAO.EditarEmpleado(emp);
                    }
                    break;
                }

                case 3: {
                    System.out.println("Digite el codigo del empleado");
                    String codigo = sc.nextLine();
                    DAO.EliminarEmpleado(codigo);
                    break;
                }

                case 4: {
                    System.out.println("Digite el codigo del empleado");
                    String codigo = sc.nextLine();
                    DAO.BuscarPorCodigo(codigo);
                    break;
                }

                case 5: {
                    System.out.println("Digite apellidos del empleado");
                    String apellidos = sc.nextLine();
                    List<Empleado> emps = DAO.BuscarPorApellidos(apellidos);
                    if (emps != null) {
                        for (Empleado e : emps) {
                            System.out.println("Empleado: " + e.getNombres() + " " + e.getApellidos());
                            System.out.println("Cédula:" + e.getCedula());
                            System.out.println("////////////////////////////////////////");
                        }
                    }
                    break;
                }

                case 6: {
                    System.out.println("Colillas con Salario Neto");
                    for (Empleado e : DAO.Empleados) {
                        System.out.println("////////////////////////////////////////");
                        System.out.println("Nombres : " + e.getNombres());
                        System.out.println("Apellidos: " + e.getApellidos());
                        System.out.println("Cedula: " + e.getCedula());
                        System.out.println("Salario Base: " + e.getSalario());
                        System.out.println("Deducción INSS : 7 % :" + ((e.getSalario() * 0.7) / 100));
                        System.out.println("Salario Neto: " + ((e.getSalario()) - ((e.getSalario() * 0.7) / 100)));
                    }
                    break;
                }
            }
            System.out.println("Desea seguir en el sistema");
            System.out.println("1. SI");
            System.out.println("0. NO");
            opcSeguir = sc.nextInt();
        }while(opcSeguir != 0);
        
    }

}
