/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

import java.util.Date;

/**
 *
 * @author samsung
 */
public class Empleado {
    private String codigo;
    private String cedula;
    private String nombres;
    private String apellidos;
    private String fecha_IngresoDate;
    private float salario;

    public Empleado() {
    }

    public Empleado(String codigo, String cedula, String nombres, String apellidos, String fecha_IngresoDate, float salario) {
        this.codigo = codigo;
        this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.fecha_IngresoDate = fecha_IngresoDate;
        this.salario = salario;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getCedula() {
        return cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getFecha_IngresoDate() {
        return fecha_IngresoDate;
    }

    public float getSalario() {
        return salario;
    }
    
    
    
    
}
